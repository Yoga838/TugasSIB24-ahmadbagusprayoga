document.write("<h1>Variabel</h1>")
document.write("<br>")
var fullname = "Ahmad Bagus Prayoga"
document.write(fullname)
document.write("<h1>Manipulasi Variabel</h1>")
fullname = "ahmad"
document.write(fullname)
document.write("<br>")
fullname ="bagus"
document.write(fullname)
document.write("<h1>Block Scope Variable</h1>")
document.write("Const tidak bisa di reassign")
document.write("var hanya mengikat scope lokal")
document.write("Let memiliki block scope dan juga hoisting")
// const not assignment variable
const name = "ahmad bagus prayoga"
console.log(name) 
// not errorr karena pengisian pertama
// name = "yoga"
// console.log(name) 
// error karena terjadi percobaan mengubah data variabel
var x = 10
if (true){
    var x = 20
    console.log("nilai x di perulangan : ",x)
}
console.log("nilai x di luar perulangan : ",x)
// tidak merubah nilai var  x diluar perulangan dikarenakan scope dari variable x hanya  mengikat local scope jadi x yang berada di luar perulangan tidak terbaca namun mendeklarasi ulang variable x di dalam perulangan

let nama = "ahmad bagus prayoga"
nama = "yoga"
console.log(nama)
if(true){
    nama = "bagus"
    console.log(nama)
}
// nama ahmad bagus prayoga dapat berganti meski ada di dalam kondisi percabangan dikarenakan let memiliki sifat block scope dan juga hoisting jadi isi dari variable dapat diganti asal berada pada block scope yang  sama dengan variabel  let dideklarasikan 
document.write("<h1>Tipe Data Number</h1>")
let bulat = 25 
let string = 'a'
let stringAngka = '4'

document.writeln(bulat/5)
//akan memiliki output 5 karena terdeteksi sebagai angka dan dibagikan dengan angka 5
document.writeln("<br>")
document.writeln(string/5)
// akan memiliki output NaN karena variabel berisi huruf atau string
document.writeln("<br>")
document.writeln(stringAngka/2)
// akan memiliki output 2 karena meski string di javascript ketika variable tersebut berisi angka maka akan dianggap sebagi angka 

document.write("<h1>Tipe Data Bigint</h1>")
let Bigint = 12345678901234567890

document.writeln("contoh Bigint : ", Bigint)

document.write("<h1>Tipe Data String</h1>")

let stringContoh = "arkatama"

document.writeln(`contoh data string : "arkatama" output : `, stringContoh)

document.write("<h1>Tipe Data Boolean</h1>")

let benarr = true
let lebihDari = 15 > 9

document.writeln("hasil dari let lebihDari = 15 > 9 menghasilkan output boolean yaitu : ", lebihDari )

document.write("<h1>Tipe Data undefined</h1>")

let kosong 

document.writeln("hasil dari let kosong tanpa mendefiniskan nilainya adalah : ", kosong)

document.write("<h1>Tipe Data null</h1>")

let kosongIsinya = null

document.writeln("hasil dari let kosongIsinya = null akan menghasilkan output : ", kosongIsinya )

document.write("<h1>Tipe Data Symbol</h1>")

let simbol = Symbol('anu')

// document.writeln(simbol)  tidak akan menampilkan isi simbol
document.writeln(simbol.description) // akan menampilkan isi dari simbol
console.log(simbol)
console.log(simbol.description)

document.write("<h1>Tipe Data Object</h1>")

let contohObjek = {
    nama : "yoga",
    umur : 21,
    menikah : false
}

document.writeln(`nama saya adalah ${contohObjek.nama} dan saya berumur ${contohObjek.umur}`)

document.write("<h1>Type conversion : string</h1>")

let temp = false 
let text = String(temp)

document.writeln(typeof(temp))
document.writeln(typeof(text))

document.write("<h1>Type conversion : Numeric</h1>")

let str = "123"
let nbr = Number(str)

document.writeln(typeof(str))
document.writeln(typeof(nbr))

document.write("<h1>Type conversion : Boolean</h1>")

document.writeln(Boolean(0))
document.writeln(Boolean(1))

document.write("<h1>Operator</h1>")
document.write("<h1>Operator</h1>")

let a  = 3
let b = 5

document.writeln(a + b)
document.writeln(a - b)
document.writeln(a * b)
document.writeln(a ** b)
document.writeln(a / b)
document.writeln(a % b)

document.write("<h1>Operator Comparasion</h1>")

document.writeln(a == b)
document.writeln(a < b)
document.writeln(a > b)
document.writeln(a != b)

document.write("<h1>Operator Logika</h1>")

document.writeln(a && b)
document.writeln(a || b)
document.writeln(!a)

document.write("<h1>Operator Bitwise</h1>")

document.writeln(a & b)
document.writeln(a | b)
document.writeln(a ^ b)
document.writeln(~a)

document.write("<h1>Operator Ternary</h1>")

document.writeln(a == b ? "sama" : "tidak sama")

document.write("<h1>Pop Up Alert</h1>")

alert("ini contoh pop up alert")

document.write("<h1>Pop Up Prompt</h1>")

let input = prompt("ini contoh pop up prompt")
document.writeln(`hasil dari prompt ${input}`)

document.write("<h1>Pop Up confirm</h1>")

let konfirmasi = confirm("ini contoh pop up confirm")

konfirmasi  ? document.writeln('terkonfirmasi') : document.writeln("tidak terkonfirmasi")
 
document.write("<h1>Percabangan : if</h1>")

if (konfirmasi){
    document.write("ini kondisi jika pop up konfirm terkonfirmasi")
}

document.write("<h1>Percabangan : if - else</h1>")

if (konfirmasi){
    document.write("ini kondisi jika pop up konfirm terkonfirmasi")
}
else{
    document.write("ini kondisi jika pop up konfirm tidak terkonfirmasi")
}

document.write("<h1>Percabangan : else if</h1>")


let contoh1 = prompt ("1+1 = ...?")

if (contoh1 = 2){
    document.write("Jawaban Benar")
}
else if (contoh > 2){
    document.write("Jawaban kelebihan")
}
else if (contoh < 2){
    document.write("Jawaban kurang")
}
else {
    document.write("Jawaban Salah")
}

document.write("<h1>Percabangan : switch</h1>")

let warna = "blue"

switch(warna){
    case "red":
        document.writeln("rose is red")
        break;
    case "blue":
        document.writeln("rose is blue")
        break;
    default:
        document.writeln("no color")
        break;
}

document.write("<h1>Perulangan : for</h1>")

for(let i = 1 ; i <5 ; i++){
    document.writeln(i)
    document.writeln("<br>")
}

document.write("<h1>Perulangan : while</h1>")

let nomor = 1

while (nomor < 5) {
    document.writeln(nomor)
    document.writeln("<br>")
    nomor++
}

document.write("<h1>Perulangan : do-while</h1>")

let temp2= 1
document.writeln('Perulangan do - while')
document.writeln('<br>')
do{
    document.writeln(temp2)
    document.writeln("<br>")
    temp2++
}while(temp2<5)

