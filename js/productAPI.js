document.addEventListener("DOMContentLoaded", fetchData);

async function fetchData() {
    try {
        const response = await fetch("https://crudcrud.com/api/bad8cc4ccc34439f8a9f23eb0bfb23e1/asa");
        const data = await response.json();
        displayData(data);
    } catch (error) {
        console.error("Error fetching data:", error);
    }
}

function displayData(data) {
    const cardsContainer = document.getElementById("cards-container");

    data.forEach(item => {
        const card = document.createElement("div");
        card.classList.add("card");

        const name = document.createElement("h2");
        name.textContent = `Nama Barang : ${item.name}`;

        const price = document.createElement("p");
        price.textContent = `Jumlah Stock Items : ${item.price}`;

        const stock = document.createElement("p");
        stock.textContent = `Stock Barang Tersedia : ${item.stock}`;

        card.appendChild(name);
        card.appendChild(price);
        card.appendChild(stock);
        cardsContainer.appendChild(card);
    });
}
