document.addEventListener("DOMContentLoaded", fetchData);

async function fetchData() {
    try {
        const response = await fetch("https://api.publicapis.org/entries");
        const data = await response.json();
        displayData(data.entries);
    } catch (error) {
        console.error("Error fetching data:", error);
    }
}

function displayData(data) {
    const cardsContainer = document.getElementById("card");

//     <!-- <div class="card-body">
    //     <h1 class="card-title mb-2">Card title</h1>
    //     <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    //     <a href="#" class="card-link">Card link</a>
    //     <a href="#" class="card-link">Another link</a>
//      </div>


    data.forEach(item => {
        const card = document.createElement("div");
        card.classList.add("card");
        card.style.width = "19rem"
        const cardBody = document.createElement("div");
        cardBody.classList.add("card-body");

        const name = document.createElement("h3");
        name.textContent = item.API;

        const price = document.createElement("p");
        price.textContent = item.Description;

        const link = document.createElement("p");
        link.textContent = `Link API : ${item.Link}`
        

        cardBody.appendChild(name);
        cardBody.appendChild(price);
        cardBody.appendChild(link);
        card.appendChild(cardBody)
        cardsContainer.appendChild(card);
    });
}
